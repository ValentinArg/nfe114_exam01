<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MultController extends Controller
{
    
    public function mult(Request $request) {
        $resultat = $request->valeur1 * $request->valeur2;
        return view('resultat_vue', ['resultat' => $resultat]);
    }
}
